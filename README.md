# Fedora Atomic Desktops SIG

SIG to co-ordinate efforts related to all Fedora Atomic Desktops (rpm-ostree
based) variants of Fedora: Silverblue, Kinoite, Sericea, Onyx.

See:
[fedoraproject.org/wiki/SIGs/AtomicDesktops](https://fedoraproject.org/wiki/SIGs/AtomicDesktops).

The manifests are currently hosted at
[pagure.io/workstation-ostree-config](https://pagure.io/workstation-ostree-config).

The cross-variants issue tracker is
[fedora/ostree/sig](https://gitlab.com/fedora/ostree/sig/-/issues).

The [fedora/ostree/buildroot](https://gitlab.com/fedora/ostree/buildroot) and
[fedora/ostree/ci-test](https://gitlab.com/fedora/ostree/ci-test) repos are
currently used to test the GitLab CI.
